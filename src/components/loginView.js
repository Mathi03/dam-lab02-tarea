import React, { Component } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";

import {
   Text,
   View,
   StyleSheet,
   Alert,
   Image,
   TextInput,
   TouchableOpacity
} from "react-native";

export class LoginView extends Component {
   constructor(props) {
      super(props);
      this.state = {
         textUser: "",
         textPassword: "",
         count: 0,
         icon: "eye-slash",
         password: true
      };
   }
   changeTextInput = text => {
      this.setState({ textUser: text });
   };
   textPassword(text) {
      this.setState({ textPassword: text });
   }
   changeIcon() {
      this.setState(prevState => ({
         icon: prevState.icon === "eye" ? "eye-slash" : "eye",
         password: !prevState.password
      }));
   }
   render() {
      return (
         <View style={styles.container}>
            <Text style={styles.title}> Login </Text>
            <Image
               source={{
                  uri:
                     "https://images.vexels.com/media/users/3/142887/isolated/preview/fc58c5ffb8c2e33fc3e15a2453189825-logo-de-la-empresa-logistica-en-crecimiento-by-vexels.png"
               }}
               style={{
                  width: 200,
                  height: 200,
                  alignSelf: "center"
               }}
            ></Image>

            <Input
               inputContainerStyle={styles.TextInput}
               onChangeText={text => this.changeTextInput(text)}
               inputStyle={{
                  paddingStart: 10,
                  color: "white"
               }}
               value={this.state.textUser}
               placeholder="username"
               leftIcon={<Icon name="user" size={24} color="white" />}
            />
            <Input
               onChangeText={this.textPassword.bind(this)}
               inputContainerStyle={styles.TextInput}
               inputStyle={{ paddingStart: 10, color: "white" }}
               value={this.state.textPassword}
               secureTextEntry={this.state.password}
               placeholder="password"
               leftIcon={<Icon name="lock" size={24} color="white" />}
               rightIcon={
                  <Icon
                     name={this.state.icon}
                     size={24}
                     color={"white"}
                     onPress={this.changeIcon.bind(this)}
                  />
               }
            />
            <TouchableOpacity
               onPress={this.onLogin.bind(this)}
               style={styles.button}
            >
               <Text style={styles.textButton}>Ingresar</Text>
            </TouchableOpacity>
         </View>
      );
   }

   onLogin() {
      Alert.alert(
         "Ingresar",
         "Te has logueado correctamente"
         /*[
            {
               text: "Aceptar",
               onPress: () => Alert.alert("Aceptar")
            },
            {
               text: "Cancelar",
               onPress: () => Alert.alert("Cancelar")
            }
         ]*/
      );
   }
}

const styles = StyleSheet.create({
   container: {
      //justifyContent: "center", //Vertical
      alignItems: "stretch", //horizontal
      flex: 1, //Unidad en el componente
      padding: 30,
      backgroundColor: "#071A1E"
   },
   button: {
      width: 250,
      height: 40,
      backgroundColor: "deepskyblue",
      borderRadius: 20, //borde curvo
      alignItems: "center",
      justifyContent: "center",
      alignSelf: "center",
      borderWidth: 1, //borde del boton,
      marginTop: 10
   },
   textButton: {
      color: "white",
      fontSize: 20
   },
   TextInput: {
      borderWidth: 1,
      //borderColor: "white",
      borderRadius: 20,
      borderBottomWidth: 1,
      //color: "white",
      marginTop: 20
   },
   title: {
      fontSize: 30,
      textAlign: "center",
      color: "white"
   }
});

module.exports = LoginView;
